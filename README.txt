$Id: 

FEEDAPI GRABBER.MODULE
--------------------------------------------------------------------------------
This module is FeedAPI addon that adds ability to grab attached files from feeds. Grabber files are 
saved as node attachment using core Upload module. Module is plug and play and it has no settings or 
configuration page.


MAINTAINERS
--------------------------------------------------------------------------------
Tamer Zoubi - <tamerzg@gmail.com>

SPONSORED BY

--------------------------------------------------------------------------------
Prosite (http://www.prosite.be)